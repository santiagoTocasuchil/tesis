
class MisTablerosPage{
    elements= {
        logo_inicio_sesion: () => cy.get('.logo'),
        logo_mi_cuenta: () => cy.get('[alt="user_profile"]'),
        boton_ayuda: () => cy.contains('Ayuda')

    }

    verificarLogoInicioDeSesion(){
        this.elements.logo_inicio_sesion().should('have.attr', 'src', '/static/media/LogoWhite.5c4b9393.svg')
    }

    clickSobreMiCuenta(){
        this.elements.logo_mi_cuenta().click();
    }
    clickSobreBotonAyuda()
    {
        this.elements.boton_ayuda().click(); 
    }

/*
    clickLogout(){
        this.elements.btn_logout().click();
    }

    clickOnDevices(){
        this.elements.btn_devices().click();
    }

    clickOnCreateDevice(){
        this.elements.btn_devices_create().click();
    }

    clickOnDeviceType(){
        this.elements.btn_device_type().click();
    }
*/    
}

export const misTablerosPage = new MisTablerosPage();
