class LandingPage{
    elements = {
        img_analitica: () => cy.get('[alt="analítica"]'),
        txt_analitica: () => cy.get('p'),

        img_usos: () => cy.get('.graphic_resource4'),
        txt_sus_usos: () => cy.get('p'),

        img_beneficios: () => cy.get('.graphic_resource5'),
        txt_beneficios: () => cy.get('p'),

        icono_twitter_home: () => cy.get('.home-icon a[href="https://twitter.com/iassoftware?lang=es"]'),
        icono_youtube_home: () => cy.get('.home-icon a[href="https://www.youtube.com/c/IASSOFTWARE/featured"]'),
        icono_instagram_home: () => cy.get('.home-icon a[href="https://www.instagram.com/iassoftware/?hl=es"]'),
        icono_facebook_home: () => cy.get('.home-icon a[href="https://es-la.facebook.com/iassoftware/"]'),
        link_twitter:() => cy.url(),

        icono_twitter_footer: () => cy.get('.socialMedia a[href="https://twitter.com/iassoftware?lang=es"]'),
        icono_youtube_footer: () => cy.get('.socialMedia a[href="https://www.youtube.com/c/IASSOFTWARE/featured"]'),
        icono_instagram_footer: () => cy.get('.socialMedia a[href="https://www.instagram.com/iassoftware/?hl=es"]'),
        icono_facebook_footer: () => cy.get('.socialMedia a[href="https://es-la.facebook.com/iassoftware/"]'),


        titulo_contacto:() => cy.get('h3'),
        link_correo_contacto:() => cy.get('li a[href="mailto:info@houndter.com"]'),
        link_tel_contacto:() => cy.get('p'),
        link_ubicacion_contacto:() => cy.get('li a[href="https://goo.gl/maps/p7CCCbWzKpV4ATSX9"]'),

        link_politicas_privacidad:() => cy.get('p')
       
        

        

    }

    visualizarImagenAnalitica()
    {
        this.elements.img_analitica().should('have.attr', 'src', '/static/media/analitica.6666e697.png')
    }
    visualizarTextoDeAnalitica()
    {
        this.elements.txt_analitica().contains('p','Descubre las tendencias en redes y medios digitales, encuentra qué opinan las personas sobre diferentes temas, analiza comportamientos históricos sobre los mismos temas, y compara la emoción de tu público digital entre diferentes temas de interés. Con Houndter, puedes transformar tu estrategia para alcanzar tus metas.')
    }
    visualizarImagenDeUsos()
    {
        this.elements.img_usos().should('have.attr', 'src', '/static/media/sus_usos.cc0f123b.png')
    }
    visualizarTextoDeUsos()
    {
        this.elements.txt_sus_usos().contains('p','Si trabajas en marketing, Houndter puede ayudarte a planificar tu estrategia de marketing midiendo la respuesta emotiva de tus potenciales usuarios y clientes.Si eres parte de una organización estatal, Houndter puede ayudarte a entender cuales son las necesidades de tu población. Podrás medir el impacto de tus políticas públicas. Si simplemente deseas estudiar el comportamiento digital de poblaciones en redes, Houndter puede entregarte métricas que te ayudarán a llevar tu análisis más allá de lo que esperas.')     
    }
    visualizarImagenDeBeneficios()
    {
        this.elements.img_beneficios().should('have.attr', 'src', '/static/media/beneficios.744c5e8b.png')
    }
    visualizarTextoDeBeneficios()
    {
        this.elements.txt_beneficios().contains('p','Encuentra qué opinan y sienten tus usuarios en diferentes medios digitales. Ahorra tiempo en la interpretación de comentarios digitales de tu público objetivo. Descubre tendencias y emociones de tu público en diferentes medios de opinión. Organiza y estudia los comportamientos históricos de tus usuarios. Compara la percepción emocional de tus usuarios entre diferentes productos o servicios.')
    }
    abrirRedsocialTwitter()
    {
       this.elements.icono_twitter_home().click();
       this.elements.icono_youtube_home().click();
       this.elements.icono_instagram_home().click();
       this.elements.icono_facebook_home().click();
    }
    
    comprobarlinkDeTwitter()
    {
        this.elements.link_twitter().should('eq','https://dev.d3o03xjtyo0zsq.amplifyapp.com/privacy')
    }
    abrirRedSocialFooter()
    {
        this.elements.icono_twitter_footer().click();
        this.elements.icono_facebook_footer().click();
        this.elements.icono_youtube_footer().click();
        this.elements.icono_instagram_footer().click();

    }

    validarInformaciónFooter()
    {
        this.elements.titulo_contacto().contains('h3','Contacto');
        this.elements.link_correo_contacto().click();
        this.elements.link_tel_contacto().contains('3014056934')
        this.elements.link_ubicacion_contacto().click();
        
    }

    validarModuloPoliticasDePrivacidad()
    {
        this.elements.link_politicas_privacidad().contains(' Política de privacidad').click();
    }
    


    
}   
export const landingPage = new LandingPage();