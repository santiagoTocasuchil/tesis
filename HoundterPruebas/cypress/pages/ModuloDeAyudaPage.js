
class ModuloDeAyudaPage
{
    
    elements={
        titulo_principal: () => cy.get('h1'),
    
        primer_titulo_secundario: () => cy.get('h3'),
        contenido_primer_parrafo: () => cy.get('p'),

        segundo_titulo_secundario: () => cy.get('h3'),
        contenido_segundo_parrafo: () => cy.get('p'),

        tercero_titulo_secundario: () => cy.get('h3'),
        contenido_tercer_parrafo: () => cy.get('p'),

        cuarto_titulo_secundario: () => cy.get('h3'),
        contenido_cuarto_parrafo: () => cy.get('p'),

        quinto_titulo_secundario: () => cy.get('h3'),
        contenido_quinto_parrafo: () => cy.get('p'),

        sexto_titulo_secundario: () => cy.get('h3'),
        contenido_sexto_parrafo: () => cy.get('p'),

        septimo_titulo_secundario: () => cy.get('h3'),
        contenido_septimo_parrafo: () => cy.get('p'),

        octavo_titulo_secundario: () => cy.get('h3'),
        contenido_octavo_parrafo: () => cy.get('p'),

        noveno_titulo_secundario: () => cy.get('h3'),
        contenido_noveno_parrafo: () => cy.get('p'),

        decimo_titulo_secundario: () => cy.get('h3'),
        contenido_decimo_parrafo: () => cy.get('p'),

        
      }
    verificarPregunta(){
        this.elements.titulo_principal().contains('h1', 'Preguntas Frecuentes')
        this.elements.primer_titulo_secundario().contains('h3', '¿Qué es Houndter')
        this.elements.contenido_primer_parrafo().contains('p','Houndter es una herramienta de análisis de percepción emocional, que se vale de tendencias en redes sociales y medios digitales, con el fin de evaluar la opinión del público en general acerca de un tema particular.') 
        this.elements.segundo_titulo_secundario().contains('h3','¿Cómo funciona Houndter?')
        this.elements.contenido_segundo_parrafo().contains('p','Houndter clasifica los comentarios extraídos de la red en cuatro categorías básicas, positivo, negativo, neutral e indefinido. A partir de estos implementa un análisis emocional basado en cinco emociones, felicidad, angustia, sorpresa, tristeza y miedo.')
        this.elements.tercero_titulo_secundario().contains('h3', '¿Qué es una nube de palabras?')
        this.elements.tercero_titulo_secundario().contains('p', 'Es una colección de palabras claves que giran en torno a un tema particular dado. El objetivo de esta es brindarle al usuario un conjunto de temas, ideas y tendencias de utilidad que facilitaran el registro y una posterior interpretación de futuros análisis de percepción emocional.')
        this.elements.cuarto_titulo_secundario().contains('h3','¿Qué es un tablero?')
        this.elements.contenido_cuarto_parrafo().contains('p','Un tablero es una colección de comentarios de redes sociales, medios digitales y un compendio de análisis estadísticos, tales como, gráficos porcentuales y de tendencias.')
        this.elements.quinto_titulo_secundario().contains('h3','¿Cómo interpretar los gráficos estadísticos?')
        this.elements.contenido_quinto_parrafo().contains('p','La primera tabla es un reporte de frecuencias del tablero presentado, donde se especifica el número de comentarios reportados y el porcentaje equivalente de estos, de acuerdo a las cinco emociones básicas. Los gráficos circulares son una representación visual de las emociones a nivel de porcentajes. Finalmente, el gráfico de barras, representa de manera visual las cuatro categorías de comentarios.')
        this.elements.sexto_titulo_secundario().contains('h3','¿Solo puedo buscar tendencias alrededor de mi ubicación?')
        this.elements.contenido_sexto_parrafo().contains('p','No, en la pestaña de "Extractor de temas de interés", y la pestaña de "Mapa" puedes visualizar tu posición actual y además usar el icono de lupa para buscar temas de interés en una nueva posición.')
        this.elements.septimo_titulo_secundario().contains('h3','¿Por qué cuando creo un tablero no lo puedo visualizar?')
        this.elements.contenido_septimo_parrafo().contains('p','Los tableros no se crean de manera inmediata, tras solicitar la creación de un nuevo tablero este puede tomar entre 3 a 10 minutos.')
        this.elements.octavo_titulo_secundario().contains('h3','¿Por qué no puedo analizar fechas anteriores a la creación de mi tablero?')
        this.elements.contenido_octavo_parrafo().contains('p','No puedes analizar fechas anteriores a la creación de tu tablero debido a que el análisis ocurre en tiempo real y solo puede recolectar información a futuro.')
        this.elements.noveno_titulo_secundario().contains('h3','¿Cuál es el límite de palabras claves que puedo usar para crear mi nube de palabras?')
        this.elements.contenido_noveno_parrafo().contains('p','El límite máximo de palabras claves que puedes usar para la creación de una nube de palabras es de 3. Además, Toda nube de palabras será creada con 30 palabra relacionadas.')
        this.elements.decimo_titulo_secundario().contains('h3','Donde puedo contactarme en caso de problemas?')
        this.elements.contenido_decimo_parrafo().contains('p','Puedes escribirnos a cualquiera de nuestras redes sociales, más abajo en la descripción. También puedes escribirnos al correo electrónico info@houndter.com o ias@ias.com.')

    }
}

export const moduloDeAyudaPage = new ModuloDeAyudaPage();
