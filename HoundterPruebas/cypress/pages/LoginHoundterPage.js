class HomeHoundterPage{
    elements = {
        txt_username: () => cy.get('[id="username"]'),
        txt_password: () => cy.get('[id="password"]'),
        btn_login: () =>  cy.contains('Ingresar')  ,  
        btn_prueba:() => cy.get('.button1'),
        btn_inicio_google:() => cy.get('.buttonGoogle')
    }

    hacerClikBotonPrueba()
    {
        this.elements.btn_prueba().click();
    }

    typeUsername(username){
        this.elements.txt_username().type(username);
    }

    typePassword(password){
        this.elements.txt_password().type(password);
    }

    clickButtonLogin(){
        this.elements.btn_login().click();
    }

    submitLogin(username, password){
        this.typeUsername(username);
        this.typePassword(password);
        this.clickButtonLogin();
    }
    iniciarConGoogle()
    {
        this.elements.btn_inicio_google().click;
    }
}

export const homeHoundterPage = new HomeHoundterPage();
