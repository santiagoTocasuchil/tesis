import { Given,When,Then } from "@badeball/cypress-cucumber-preprocessor";
import "cypress-mochawesome-reporter/register";
import { landingPage } from "../../pages/LandingPage";

Given ("Ingresar al home de houndter2", (tablaDeDatos) => {
    
    tablaDeDatos.hashes().forEach((row) => {
        const [width, height] = row.resolucion.split('x');
        cy.viewport(parseInt(width), parseInt(height));
        });
        cy.visit('https://dev.d3o03xjtyo0zsq.amplifyapp.com/');
      });

When ("Se visualicen los iconos de las resdes sociales de la empresa en el home", () => {

    landingPage.abrirRedsocialTwitter();
   
});

When ("Se visualicen los iconos de las resdes sociales de la empresa en el footer", () => {

    landingPage.abrirRedSocialFooter();
    
});

When ("Se dirija al modulo de politicas de privacidad", () => {
    landingPage.validarModuloPoliticasDePrivacidad();
})

When ("Se visualice la información de contacto de la empresa", () => {
    landingPage.validarInformaciónFooter()
})

Then ("Se deben abrir las redes sociales de las empresa en nuevas pestañas", () => {

    landingPage.comprobarlinkDeTwitter();
    
});