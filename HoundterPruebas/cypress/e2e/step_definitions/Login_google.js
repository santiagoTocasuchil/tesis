import { Given,When,Then } from "@badeball/cypress-cucumber-preprocessor";
import "cypress-mochawesome-reporter/register";
import {homeHoundterPage} from "../../pages/LoginHoundterPage"
import {misTablerosPage} from "../../pages/MisTablerosPage"

Given ("Ingreso al landing page de Houndter", (dataTable) => {
    
dataTable.hashes().forEach((row) => {
    const [width, height] = row.resolucion.split('x');
    cy.viewport(parseInt(width), parseInt(height));
    });
    cy.visit('https://dev.d3o03xjtyo0zsq.amplifyapp.com/');
    homeHoundterPage.hacerClikBotonPrueba();
  });


  When ("Ingreso con la opcion de google",()=>{
    
    homeHoundterPage.iniciarConGoogle();

  });
  
  Then ("Se debe visualizar la opción para crear nube de palabras",()=>{

    misTablerosPage.verificarLogoInicioDeSesion();

  })