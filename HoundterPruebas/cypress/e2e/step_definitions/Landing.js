import { Given,When,Then } from "@badeball/cypress-cucumber-preprocessor";
import "cypress-mochawesome-reporter/register";
import { landingPage } from "../../pages/LandingPage";

Given ("Ingresar al home de houndter", (Table) => {
    
    Table.hashes().forEach((row) => {
        const [width, height] = row.resolucion.split('x');
        cy.viewport(parseInt(width), parseInt(height));
        });
        cy.visit('https://dev.d3o03xjtyo0zsq.amplifyapp.com/');
      });

When ("Se visualice la imagen de analitica de percepción emocional, sus usos y beneficios", () => {

        landingPage.visualizarImagenAnalitica();
        landingPage.visualizarImagenDeUsos();
        landingPage.visualizarImagenDeBeneficios();

    });

Then ("Se debe visualizar el texto de forma correcta sin errores ortograficos", () => {
   
    landingPage.visualizarTextoDeAnalitica();
    landingPage.visualizarImagenDeUsos();
    landingPage.visualizarImagenDeBeneficios();

});
/*
Given ("Ingresar al home de houndter", (table) => {
    
    table.hashes().forEach((row) => {
        const [width, height] = row.resolucion.split('x');
        cy.viewport(parseInt(width), parseInt(height));
        });
        cy.visit('https://dev.d3o03xjtyo0zsq.amplifyapp.com/');
      });

When ("Se visualicen los iconos de las resdes sociales de la empresa", () => {

    landingPage.verificarIconosRedesSociales();
});

Then ("Se dirija al link de la red social corresponiente", () => {

    landingPage.dirigirATwitter();
});
 */    