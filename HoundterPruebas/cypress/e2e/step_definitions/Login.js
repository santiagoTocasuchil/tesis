import { Given,When,Then } from "@badeball/cypress-cucumber-preprocessor";
import "cypress-mochawesome-reporter/register";
import { homeHoundterPage } from "../../pages/LoginHoundterPage";
import { misTablerosPage } from "../../pages/MisTablerosPage";

Given ("Ingresar al home de houndter con resolucion", (dataTable) => {
    
dataTable.hashes().forEach((row) => {
    const [width, height] = row.resolucion.split('x');
    cy.viewport(parseInt(width), parseInt(height));
    });
    cy.visit('https://dev.d3o03xjtyo0zsq.amplifyapp.com/');
    homeHoundterPage.hacerClikBotonPrueba();
  });
  
  When ("Ingreso las credenciales correctas",(table)=>{
    table.hashes().forEach((row)=>{
    homeHoundterPage.submitLogin(row.username, row.password);
    });
  });
  
  Then ("Se debe dirigir al home con el resumen de los tableros",()=>{

    misTablerosPage.verificarLogoInicioDeSesion();

  })
