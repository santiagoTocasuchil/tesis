Feature:Logos de las redes sociales

Scenario: Validar que los logos de las redes sociales se visualicen y dirigan a la red social correspondiente <deviceName>
    Given Ingresar al home de houndter2
    |resolucion|
    |<resolucion>|
    When Se visualicen los iconos de las resdes sociales de la empresa en el home
    And Se visualicen los iconos de las resdes sociales de la empresa en el footer
    And Se visualice la información de contacto de la empresa
    And Se dirija al modulo de politicas de privacidad
    Then Se deben abrir las redes sociales de las empresa en nuevas pestañas
    
    Examples:
    |deviceName | resolucion |
    |Acer Chromebook|1376x768|
    