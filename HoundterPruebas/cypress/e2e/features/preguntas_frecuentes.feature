Feature: Modulo preguntas frecuentes

  Scenario: Validar el modulo de preguntas frecuentes con diferentes resoluciones <deviceName>
    Given Ingresar al home de houndter con resolucion diferente
    |resolucion|
    |<resolucion>|
    When Ingreso las credenciales de loguin correctas
    |username|password|
    |<username>|<password>|
    And Ingreso en el modulo de ayuda
    Then Se debe visualizar la informacion de preguntas frecuentes
    Examples:
    |deviceName | resolucion | username| password|
    |Acer Chromebook|1372x768|juancrv345@gmail.com|pruebadev$_!9123|
    |Macbook 15 |1440x900|juancrv345@gmail.com|pruebadev$_!9123|
    |Macbook 16|1536x960|juancrv345@gmail.com|pruebadev$_!9123|



     
     