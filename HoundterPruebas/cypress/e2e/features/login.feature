Feature: Login en Houndter

  Scenario: Validar que el login sea exitoso en diferentes resoluciones <deviceName>
    Given Ingresar al home de houndter con resolucion 
    |resolucion|
    |<resolucion>|
    When Ingreso las credenciales correctas
    |username|password|
    |<username>|<password>|
    Then Se debe dirigir al home con el resumen de los tableros

    Examples:
    |deviceName | resolucion | username| password|
    |Acer Chromebook|1376x768|juancrv345@gmail.com|pruebadev$_!9123|
    |Macbook 15 |1440x900|juancrv345@gmail.com|pruebadev$_!9123|
    |Macbook 16|1536x960|juancrv345@gmail.com|pruebadev$_!9123|


    
    Scenario Outline: Test Case: <nameTest>
        When Ingreso las credenciales incorrectas
            |username|password|
            |<username>|<password>|
        Then Visualizo el siguiente mensaje de error
            |                           message                                                                                                            |
            |¡Error! usuario o contraseña incorrecta. Verifica si ya te encuentras registado, si aún no lo estás, descarga nuestra aplicación y registrate.|
    Examples:
                |               nameTest                    |       username            |           password                                    |
                |2- Valid from error message                |mateolara315@gmail.com     |estoNoEx1STE                                           |
                |3- Nonexistent username and valid password |duvier123martinez@gmail.com|Mateolara-15                                           |
                |4- Username without @ and valid password   |mateolara315gmail.com      |Mateolara-15                                           |

    
    
      
